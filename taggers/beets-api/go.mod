module beets

go 1.16

require (
	github.com/cespare/xxhash v1.1.0
	github.com/gorilla/websocket v1.4.2
)
