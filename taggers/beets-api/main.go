package main

import (
	"fmt"
	"log"
	"net/http"
	"os"
	"os/exec"
	"strconv"
	"strings"
	"time"

	"github.com/cespare/xxhash"
	"github.com/gorilla/websocket"
)

const CLIENTID = "x-client-id"

const WelcomeBaneer = `
#########################################################
#    ____            _                     _____ _____ 
#   |  _ \          | |              /\   |  __ \_   _|
#   | |_) | ___  ___| |_ ___ ______ /  \  | |__) || |  
#   |  _ < / _ \/ _ \ __/ __|______/ /\ \ |  ___/ | |  
#   | |_) |  __/  __/ |_\__ \     / ____ \| |    _| |_ 
#   |____/ \___|\___|\__|___/    /_/    \_\_|   |_____|
#                                                      
#                                                      
### Usage :
#  [cmd] ...args
#
#  Cmd = ls / ps / make / docker
#
#  Note : All commands will be sanitized before execution
`

var AuthorizedCommands = []string{
	"ls",
	"ps",
	"make",
	"ytdl",
	"docker",
}

func clean(s []byte) string {
	j := 0
	for _, b := range s {
		if ('a' <= b && b <= 'z') ||
			('A' <= b && b <= 'Z') ||
			('0' <= b && b <= '9') ||
			b == ' ' {
			s[j] = b
			j++
		}
	}
	return strings.ToLower(string(s[:j]))
}

func sanityzeMessage(cmds []string) bool {
	var res = false

	first := cmds[0]
	if first != "ytdl" {
		for el := range cmds {
			cmds[el] = clean([]byte(cmds[el]))
		}
	}
	log.Printf(
		"[INFO] Got cmds %+v\n",
		cmds,
	)
	for el := range AuthorizedCommands {
		fmt.Printf("[INFO] Comparing [%s] to [%s]\n", first, AuthorizedCommands[el])
		if first == AuthorizedCommands[el] {
			res = true
			break
		}
	}
	return res
}

var PORT = func() string {
	var port = os.Getenv("PORT")
	if port == "" {
		return "8080"
	}
	return port
}()

type ClientIDCtx string

var pool = map[string]int{}

var upgrader = websocket.Upgrader{
	HandshakeTimeout: 2 * time.Second,
	ReadBufferSize:   1024,
	WriteBufferSize:  1024,
	// Error: func(w http.ResponseWriter, r *http.Request, status int, reason error) {
	// 	// status = http.StatusUnauthorized
	// 	// reason = errors.New("not allowed dude, see ya")
	// 	jsonResp := map[string]string{"error": reason.Error()}
	// 	body, _ := json.Marshal(jsonResp)
	// 	w.Write(body)
	// },
	CheckOrigin: func(r *http.Request) bool {
		originHdr := r.Header.Get("Origin")
		fmt.Printf(
			"[INFO] Got origin [%s] from address [%s] with user-agent [%s]\n",
			originHdr,
			r.RemoteAddr,
			r.UserAgent())
		clientID := xxhash.Sum64String(originHdr + r.Host + r.UserAgent())
		clIDStr := strconv.FormatUint(clientID, 10)
		// r = r.WithContext(context.WithValue(r.Context(), ClientIDCtx("client_id"), clientID))
		fmt.Printf("[INFO] Got client ID %v\n", clIDStr)
		r.Header.Set(CLIENTID, clIDStr)
		pool[CLIENTID] = 0
		return true
	},
}

func echo(w http.ResponseWriter, r *http.Request) {
	c, err := upgrader.Upgrade(w, r, nil)
	if err != nil {
		log.Print("upgrade:", err)
		return
	}
	defer c.Close()
	for {
		var (
			mt       int
			message  []byte
			err      error
			clientID = r.Header.Get(CLIENTID)
		)

		if pool[clientID] == 0 {
			if err = c.WriteMessage(1, []byte(WelcomeBaneer)); err != nil {
				log.Printf("[ERROR] error writing message : %s\n", err.Error())
				break
			}
		}
		if mt, message, err = c.ReadMessage(); err != nil {
			if !websocket.IsCloseError(err, 1006) {
				log.Printf("[ERROR] error reading message : %s\n", err.Error())
			} else {
				log.Printf(
					"[INFO] Client [%s] on (%s) close connection\n",
					clientID,
					r.UserAgent(),
				)
			}
			break
		}

		pool[clientID]++
		log.Printf(
			"[INFO] received message N° %v : [%s] from client-id [%s]\n",
			pool[clientID],
			message,
			clientID,
		)
		cmds := strings.Split(string(message), " ")
		if cmds[0] != "" && sanityzeMessage(cmds) {
			// TODO(me): Sanitize command based on a whitelist
			if cmds[0] == "ytdl" {
				cmds[0] = "./" + "ytdl"
			}
			cmd := exec.Command(cmds[0], cmds[1:]...)
			// out, err := cmd.CombinedOutput()
			stdout, err := cmd.StdoutPipe()
			if err != nil {
				log.Printf("[ERROR] cmd.Run() failed with %s\n", err.Error())
				break
			}
			cmd.Stderr = cmd.Stdout
			stdIndPipe, err := cmd.StdinPipe()
			if err != nil {
				log.Printf("[ERROR] cmd.StdinPipe() failed with %s\n", err.Error())
				break
			}
			defer stdIndPipe.Close()
			if err = cmd.Start(); err != nil {
				log.Printf("[ERROR] cmd.Run() failed with %s\n", err.Error())
				break
			}

			for {
				tmp := make([]byte, 1024)
				_, err := stdout.Read(tmp)
				if err != nil {
					log.Printf("[ERROR] error reading output : %s\n", err.Error())
					break
				}
				if err = c.WriteMessage(mt, tmp); err != nil {
					log.Printf("[ERROR] error writing message : %s\n", err.Error())
					break
				} else {
					log.Printf("[INFO] sending response :\n[%s]\n", string(tmp))
					// break
				}
				if mt, message, err = c.ReadMessage(); err != nil {
					if !websocket.IsCloseError(err, 1006) {
						log.Printf("[ERROR] error reading message : %s\n", err.Error())
					} else {
						log.Printf(
							"[INFO] Client [%s] on (%s) close connection\n",
							clientID,
							r.UserAgent(),
						)
					}
					break
				}
				if len(message) > 0 {
					fmt.Printf("[INFO] Piping %s to stdin\n", string(message))
					if _, err = stdIndPipe.Write(message); err != nil {
						fmt.Printf("[ERROR] Error writing to stdin : %s\n", err.Error())
					}

				}

			}
			cmd.Wait()
		} else {
			out := []byte("\n\t⚠️ Missing or invalid command\n")
			if err = c.WriteMessage(mt, out); err != nil {
				log.Printf("[ERROR] error writing message : %s\n", err.Error())
				break
			}
		}

	}
}

func main() {
	addr := ":" + PORT
	log.SetFlags(0)
	http.HandleFunc("/echo", echo)
	fmt.Printf("[INFO] Server listening on %s\n", addr)
	log.Fatal(http.ListenAndServe(addr, nil))
}
