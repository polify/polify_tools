module uploader

go 1.16

require (
	github.com/cheggaaa/pb v1.0.29
	github.com/fatih/color v1.10.0 // indirect
	github.com/gabriel-vasile/mimetype v1.2.0
	github.com/joho/godotenv v1.3.0
	github.com/mattn/go-runewidth v0.0.12 // indirect
	github.com/minio/minio-go/v7 v7.0.10
	github.com/rivo/uniseg v0.2.0 // indirect
	golang.org/x/sys v0.0.0-20210403161142-5e06dd20ab57 // indirect
)
