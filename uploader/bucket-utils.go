package main

import (
	"context"
	"log"
	"os"

	"github.com/minio/minio-go/v7"
	"github.com/minio/minio-go/v7/pkg/credentials"
)

// Bucket ...
type Bucket struct {
	Name   string
	Client *minio.Client
}

func MustNewBucket() Bucket {
	return Bucket{
		Name:   os.Getenv("bucket-name"),
		Client: mustsetupS3Client(),
	}
}

func mustsetupS3Client() (s3Client *minio.Client) {
	var err error
	var useSSL = true
	var endpoint = os.Getenv("bucket-endpoint")
	var accessKeyID = os.Getenv("access-key")
	var secretAccessKey = os.Getenv("secret-key")
	var clientLocation = os.Getenv("client-region")

	log.Printf("[INFO] Will try to connect to bucket at %s\n", endpoint)
	if s3Client, err = minio.New(endpoint, &minio.Options{
		Creds:  credentials.NewStaticV4(accessKeyID, secretAccessKey, ""),
		Secure: useSSL,
		Region: clientLocation,
	}); err != nil {
		log.Fatalln(err)
	}
	return
}

func (b *Bucket) MustUpsertBucket(bucketName string) {
	var err error
	var ctx = context.Background()
	bucketLocation := os.Getenv("bucket-region")
	if err = b.Client.MakeBucket(
		ctx,
		bucketName,
		minio.MakeBucketOptions{Region: bucketLocation},
	); err != nil {
		// Check to see if we already own this bucket (which happens if you run this twice)
		exists, errBucketExists := b.Client.BucketExists(ctx, bucketName)
		if errBucketExists == nil && exists {
			log.Printf("[INFO] Bucket %s already exist\n", bucketName)
		} else {
			log.Fatalf("[ERROR] Failed to make bucket : %s\n", err.Error())
		}
	} else {
		log.Printf("[INFO] Successfully created bucket %s\n", bucketName)
	}
}
