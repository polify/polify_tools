package main

import (
	"errors"
	"log"
	"os"
	"path/filepath"
	"strings"

	"github.com/joho/godotenv"
)

func main() {
	var err error
	var localFPaths = []string{}
	if len(os.Args) != 2 {
		log.Fatalf("USAGE : %s [filepath]\n", os.Args[0])
	}
	if err = godotenv.Load(); err != nil {
		log.Fatal("Error loading .env file")
	}

	bucket := MustNewBucket()

	localFPath := os.Args[1]
	info, err := os.Stat(localFPath)
	if errors.Is(err, os.ErrNotExist) {
		log.Fatalf("File %s does not exist.\n", localFPath)
	}
	if info.IsDir() {
		if err = filepath.Walk(localFPath,
			func(path string, info os.FileInfo, err error) error {
				if err != nil {
					return err
				}
				if !info.IsDir() {
					localFPaths = append(localFPaths, path)
				}

				return nil
			}); err != nil {
			log.Fatalln(err)
		}
	} else {
		if strings.Contains(localFPath, "./") {
			localFPath = strings.ReplaceAll(localFPath, "./", "")
		}
		localFPaths = append(localFPaths, localFPath)
	}
	log.Printf("[INFO] Found %v files to upload\n", len(localFPaths))
	for i := range localFPaths {
		el := MustNewFileInfos(localFPaths[i])
		log.Printf("[INFO] Checking file %v/%v\n", i, len(localFPaths))
		if !el.FileExist(bucket.Client, bucket.Name) {
			log.Printf(
				"[INFO] Will try to upload file %v/%v : '%s' of type '%s' and size '%v'\n",
				i,
				len(localFPaths),
				el.FileName, el.ContentType, el.FileSize,
			)
			el.MustUploadFile(bucket.Client, bucket.Name)
		}

	}

}
