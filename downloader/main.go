package main

import (
	"context"
	"fmt"
	"io"
	"log"
	"os"
	"path/filepath"
	"strconv"

	"github.com/joho/godotenv"
	"github.com/minio/minio-go/v7"
	"github.com/minio/minio-go/v7/pkg/credentials"
)

const (
	TB int64 = 1000000000000
	GB int64 = 1000000000
	MB int64 = 1000000
	KB int64 = 1000
)

// ToHr convert a filesize in byte to human readable string with unit
func ToHr(length int64, decimals int) (out string) {
	var unit string
	var i int64
	var remainder int64

	// Get whole number, and the remainder for decimals
	if length > TB {
		unit = "TB"
		i = length / TB
		remainder = length - (i * TB)
	} else if length > GB {
		unit = "GB"
		i = length / GB
		remainder = length - (i * GB)
	} else if length > MB {
		unit = "MB"
		i = length / MB
		remainder = length - (i * MB)
	} else if length > KB {
		unit = "KB"
		i = length / KB
		remainder = length - (i * KB)
	} else {
		return strconv.FormatInt(length, 10) + " B"
	}

	if decimals == 0 {
		return strconv.FormatInt(i, 10) + " " + unit
	}

	// This is to calculate missing leading zeroes
	width := 0
	if remainder > GB {
		width = 12
	} else if remainder > MB {
		width = 9
	} else if remainder > KB {
		width = 6
	} else {
		width = 3
	}

	// Insert missing leading zeroes
	remainderString := strconv.FormatInt(remainder, 10)
	for iter := len(remainderString); iter < width; iter++ {
		remainderString = "0" + remainderString
	}
	if decimals > len(remainderString) {
		decimals = len(remainderString)
	}

	return fmt.Sprintf("%d.%s %s", i, remainderString[:decimals], unit)
}

func main() {
	var err error
	var s3Client *minio.Client
	var ctx = context.Background()

	if len(os.Args) != 2 {
		log.Fatalf("USAGE : %s [filepath]\n", os.Args[0])
	}
	if err = godotenv.Load(); err != nil {
		log.Fatal("Error loading .env file")
	}
	var useSSL = true
	var endpoint = os.Getenv("bucket-endpoint")
	var accessKeyID = os.Getenv("access-key")
	var secretAccessKey = os.Getenv("secret-key")
	var clientLocation = os.Getenv("client-region")

	log.Printf("[INFO] Will try to connect to bucket at %s\n", endpoint)
	// Initialize s3 client object.
	if s3Client, err = minio.New(endpoint, &minio.Options{
		Creds:  credentials.NewStaticV4(accessKeyID, secretAccessKey, ""),
		Secure: useSSL,
		Region: clientLocation,
	}); err != nil {
		log.Fatalln(err)
	}
	log.Println("[INFO] Connection succeeded")

	var reader *minio.Object
	var localFile *os.File
	var stat minio.ObjectInfo
	var filePath, fileName string
	var bucketName = os.Getenv("bucket-name")

	filePath = os.Args[1]
	fileName = filepath.Base(filePath)
	if reader, err = s3Client.GetObject(ctx,
		bucketName,
		fileName,
		minio.GetObjectOptions{},
	); err != nil {
		log.Fatalln(err)
	}
	defer reader.Close()
	log.Printf("[INFO]: Succesfully found bucket file %s\n", fileName)

	if localFile, err = os.Create(filePath); err != nil {
		log.Fatalf("[ERROR] Failed to create file %s : %s\n", filePath, err.Error())
	}
	defer localFile.Close()

	if stat, err = reader.Stat(); err != nil {
		log.Fatalln(err)
	}
	log.Printf("[INFO] Will copy reader file of size %s to %s\n", ToHr(stat.Size, 2), filePath)
	if _, err := io.CopyN(localFile, reader, stat.Size); err != nil {
		log.Fatalln(err)
	}
	log.Printf("[INFO] Successfully downloaded %s of size %s\n", fileName, ToHr(stat.Size, 2))
}
