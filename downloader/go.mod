module downloader

go 1.16

require (
	github.com/joho/godotenv v1.3.0
	github.com/minio/minio-go/v7 v7.0.10
)
